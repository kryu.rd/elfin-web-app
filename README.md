Для проверки работоспособности приложения:

- GET запрос по пути "/"
- Проверить отклик livenessProbe

Для того, чтобы рестартануть все деплойменты содержащие слово "test":

#### Способ №1
```bash
#!/bin/bash

deployments=$(kubectl get deployments -o=json | jq -r '.items[] | select(.metadata.name | contains("test")) | .metadata.name')

for deployment in $deployments; do
    echo "Рестарт деплоймента: $deployment"
    kubectl rollout restart deployment $deployment
done
```

#### Способ №2
```bash
kubectl get deployments -o=name | grep "test" | xargs -I {} kubectl rollout restart {}
```

Восстановление подов вызвано наличием ReplicaSet и статическими манифестами. Также запуск некоторых компонентов кластера может быть реализовано через сервисы systemd.
