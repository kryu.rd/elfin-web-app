FROM python:3.9-slim

WORKDIR /usr/src/web-app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

RUN adduser --disabled-password --uid 1001 user

RUN chown -R user:user /usr/src/web-app

USER user

COPY src/ ./web-app

EXPOSE 8000

CMD ["python", "web-app/main.py"]